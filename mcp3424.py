import smbus
import time

class MCP3424:
    def __init__(self, i2c_bus=1, address=0x6E, config_base=0x10, R1=30000, R2=20000, reference_voltage=2.048):
        self.bus = smbus.SMBus(i2c_bus)
        self.address = address
        self.config_base = config_base
        self.R1 = R1
        self.R2 = R2
        self.reference_voltage = reference_voltage

    def read_channel(self, channel):
        config = self.config_base | (channel << 5)
        self.bus.write_byte(self.address, config)

        # Introduce a small delay before reading data
        time.sleep(0.1)

        # Read data back from 0x00(0), 3 bytes
        data = self.bus.read_i2c_block_data(self.address, 0x00, 3)

        # Combine the bytes into an 18-bit value
        raw_value = ((data[0] & 0x03) << 16) | (data[1] << 8) | data[2]

        # Convert 18-bit digital value to voltage
        voltage_mcp3424 = (raw_value / (2**18 - 1)) * self.reference_voltage

        # Calculate the actual input voltage using the voltage divider formula
        voltage_input = voltage_mcp3424 * (1 + self.R1 / self.R2)

        return voltage_mcp3424, voltage_input

# if __name__ == "__main__":
#     try:
#         mcp3424 = MCP3424()

#         while True:
#             for channel in range(4):
#                 voltage_mcp3424, voltage_input = mcp3424.read_channel(channel)
#                 print("Channel {}: MCP3424 Voltage = {:.4f} V, Actual Input Voltage = {:.4f} V".format(channel + 1, voltage_mcp3424, voltage_input))

#             time.sleep(1)

#     except KeyboardInterrupt:
#         print("\nProgram terminated by user.")


