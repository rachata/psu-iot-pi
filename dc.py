import minimalmodbus
import serial
import time

class ModbusDeviceDC:
    def __init__(self, port='/dev/ttyUSB0', slave_address=16, baudrate=9600):
        self.instr = minimalmodbus.Instrument(port, slave_address, debug=True)
        self.instr.serial.baudrate = baudrate
        self.instr.serial.bytesize = 8
        self.instr.serial.parity = serial.PARITY_NONE
        self.instr.serial.stopbits = 2
        self.instr.serial.timeout = 2

    def read_register(self, address, functioncode):
        result = self.instr.read_register(address, functioncode=functioncode)
        return result
    
    def read_long(self, address, functioncode):
        result = self.instr.read_long(address, functioncode=functioncode)
        return result
    

    
    def read_float(self, address, functioncode):
        result = self.instr.read_float(address, functioncode=functioncode)
        return result
    

    def write_register(self, address, value, functioncode):
        self.instr.write_register(address, value, functioncode=functioncode)

    def read_voltage(self):
        voltage_address = 0x0000
        voltage_functioncode = 4
        result = self.read_register(voltage_address, voltage_functioncode)
        return result * 0.01

    def read_current(self , factor):
        current_address = 0x0001
        current_functioncode = 4
        result = self.read_register(current_address, current_functioncode)
        return result * factor

    def read_power(self):
        power_address = 0x0002
        power_functioncode = 4
        result = self.read_register(power_address, power_functioncode)
        return result

# if __name__ == "__main__":
#     # Example: Initializing ModbusDeviceDC with a specific port
#     modbus_device_dc = ModbusDeviceDC(port='/dev/ttyUSB0')

#     # Example: Reading current with factor
#     current = modbus_device_dc.read_current()
#     print("Current:", current, "A")

#     time.sleep(100 / 1000)

#     # Example: Writing to a register
#     # modbus_device_dc.write_register(0x0003, 0x0010, functioncode=6)
