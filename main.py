import io
import sys
import fcntl
import time
import copy
import string
import paho.mqtt.client as mqtt
import os
import time
import json

import RPi.GPIO as GPIO
import time

from atlas.AtlasI2C import AtlasI2C
from dc import ModbusDeviceDC

from mcp3424 import MCP3424


from water_level.DFRobot_RaspberryPi_A02YYUW import DFRobot_A02_Distance as Board



DEVICE_ID = "aebf037097fa11eea683eef48e200650"
DEVICE_IDS = 1

broker_address = "128.199.196.64"
broker_port = 1883
topic = "aqua/sensor/"+str(DEVICE_ID)
username = "psu-mqtt"
password = "Zd4h&M)Ryg!&JpfPJkzXI)U%"



slave_address_dc = 16
slave_baudrate_dc = 9600

slave_address_temp = 2
slave_baudrate_temp = 9600

channel_mcp3424 = 4 - 1


slave_address_pressure = 1
slave_baudrate_pressure = 9600



RELAY_PIN = 23


TIME_ON = 5
TIME_OFF = 5

dataDevice = {
  
    "deviceIds" : DEVICE_IDS,
    "deviceId": DEVICE_ID,
    "ph": 0, 
    "tempWater":  0,
    "do": 0,
    "conductivity": 0,
    "temp": 0,
    "humidity": 0,
    "atmosphericPressure": 0,
    "turbidity": 0,
    "levelWater": 0,
    "salinity": 0,
    "location" :  [7.17317,100.62574], 
    "battery" : 0
      
}





def calculate_atmospheric_pressure_fixed_temperature(temperature_C):
 
    # Fixed relationship: 25°C corresponds to 1035 mbar
    temperature_reference = 25
    atmospheric_pressure_reference = 1035

    # Linear interpolation
    pressure = atmospheric_pressure_reference + (temperature_C - temperature_reference) * 0.1

    return pressure



def get_distance():
  
    board = Board()
 
    dis_min = 0 

    dis_max = 4500 
    board.set_dis_range(dis_min, dis_max)

    dis = board.getDistance()


    if board.last_operate_status == board.STA_OK:
        return dis
    elif board.last_operate_status == board.STA_ERR_CHECKSUM:
        return -1
    elif board.last_operate_status == board.STA_ERR_SERIAL:
        return -1
    elif board.last_operate_status == board.STA_ERR_CHECK_OUT_LIMIT:
        return -1
    elif board.last_operate_status == board.STA_ERR_CHECK_LOW_LIMIT:
        return -1
    elif board.last_operate_status == board.STA_ERR_DATA:
        return -1



def get_devices():

    
    
    device = AtlasI2C()
    device_address_list = device.list_i2c_devices()
    device_list = []
    
    for i in device_address_list:
        device.set_i2c_address(i)
        response = device.query("I")
        try:
            moduletype = response.split(",")[1] 
            response = device.query("name,?").split(",")[1]
        except IndexError:
            print(">> WARNING: device at I2C address " + str(i) + " has not been identified as an EZO device, and will not be queried") 
            continue
        device_list.append(AtlasI2C(address = i, moduletype = moduletype, name = response))
    return device_list 


def extract_float_from_string(data):
    # Find the index of the first null character
    null_index = data.find('\x00')

    # Extract the substring up to the null character
    number_str = data[:null_index]

    # Convert the substring to a floating-point number
    result = float(number_str)

    return result



def round_with_decimal(number, decimal_places):

    return round(number, decimal_places)




       



def calculate_ntu(voltage):

    ntu = -1120.4 * voltage**2 + 5742.3 * voltage - 4352.9
    return ntu



def start_work(on_time_sec, off_time_sec) :
    try:

    # Set ON Sensor Atlas
        GPIO.setmode(GPIO.BCM)

        pins = [13 , 19 , 5 , 6]

        for pin in pins:
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, GPIO.HIGH)



        GPIO.setup(RELAY_PIN, GPIO.OUT)
        GPIO.output(RELAY_PIN, GPIO.LOW)  


        time.sleep(1)

        client = mqtt.Client()

        client.username_pw_set(username, password)

        client.connect(broker_address, broker_port)

        while True:    


            device_list = get_devices()


            GPIO.output(RELAY_PIN, GPIO.HIGH)
        
            time.sleep(on_time_sec)
            

            for dev in device_list:
                dev.write("R")
            time.sleep(1)
            for dev in device_list:

                name = dev.get_device_info().split(' ')[0]         





                sensor = dev.readPSU()

                if name == 'DO' :

                        

                    dataDevice['do'] = round_with_decimal( extract_float_from_string(sensor) , 2)

                if name == 'pH' :

                    dataDevice['ph'] =  round_with_decimal( extract_float_from_string(sensor)  , 2)

                if name == 'EC' :

                    dataDevice['conductivity'] =  round_with_decimal( extract_float_from_string(sensor)  , 2)
                    dataDevice['salinity'] =  dataDevice['conductivity']

                if name == 'RTD' :

                    dataDevice['tempWater'] =  round_with_decimal( extract_float_from_string(sensor)    , 2)
        
                

                                

            time.sleep(1)

        # Set OFF Sensor Atlas

            for pin in pins:
                GPIO.setup(pin, GPIO.OUT)
                GPIO.output(pin, GPIO.LOW)






        #  Get  distance
        
            distance = get_distance()
            
            distance = distance * 0.001


            dataDevice['levelWater'] = round_with_decimal (distance , 2)





        # Get DC


            modbus_device_dc = ModbusDeviceDC(port='/dev/ttyUSB0' , slave_address = slave_address_dc , baudrate=slave_baudrate_dc)


            voltage = modbus_device_dc.read_voltage()
            time.sleep(100/1000)
            current = modbus_device_dc.read_current(factor= 1)
            time.sleep(100/1000)

            dataDevice['battery'] = round_with_decimal (voltage , 2)
            




        #  Get Temp Hum

            modbus_device_temp = ModbusDeviceDC(port='/dev/ttyUSB0' , slave_address = slave_address_temp , baudrate= slave_baudrate_temp)

            temp = modbus_device_temp.read_register(address=0x0001 , functioncode=4)

            temp = temp * 0.1

            dataDevice['temp'] = round_with_decimal ( temp , 2)


            humidity = modbus_device_temp.read_register(address=0x0000 , functioncode=4)

            humidity = humidity * 0.1

            dataDevice['humidity'] =  round_with_decimal ( humidity , 2)



        #  Get MCP3424






            mcp3424 = MCP3424()


            voltage_mcp3424, voltage_input = mcp3424.read_channel(channel_mcp3424)

            ntu = calculate_ntu(voltage_input)

            ntu = round_with_decimal(ntu , 2)

            # print(ntu)
            # print(voltage_input)


            dataDevice['turbidity']  = ntu

            print("Channel {}: MCP3424 Voltage = {:.4f} V, Actual Input Voltage = {:.4f} V".format(channel_mcp3424, voltage_mcp3424, voltage_input))




        #  Get pressure

            modbus_device_pressure = ModbusDeviceDC(port='/dev/ttyUSB0' , slave_address = slave_address_pressure , baudrate= slave_baudrate_pressure)

            pressure = modbus_device_pressure.read_register(address=0x000A , functioncode=3)

        


            

            pressure = calculate_atmospheric_pressure_fixed_temperature(temp)

            dataDevice['atmosphericPressure'] = round_with_decimal(pressure , 2)


            print(dataDevice)



            json_message = json.dumps(dataDevice)


            print(topic)

            client.publish(topic, json_message)


            GPIO.output(RELAY_PIN, GPIO.LOW)
            time.sleep(off_time_sec)


    except KeyboardInterrupt:

        GPIO.cleanup()

    finally:


        for pin in pins:
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, GPIO.LOW)


        GPIO.cleanup()



start_work(TIME_ON , TIME_OFF)





